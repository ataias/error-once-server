import cors from 'cors'
import express from 'express'
const app = express()
const port = 3000

const requestCodesToPass = new Set()

app.use(cors())

app.all('/rest/:errorCode', (req, res) => {
  const errorCode = Number(req.params.errorCode)
  if (errorCode >= 400 && errorCode <= 599 && !requestCodesToPass.has(errorCode)) {
    requestCodesToPass.add(errorCode)
    setTimeout(() => {
      requestCodesToPass.delete(errorCode)
    }, 8000)
    res.status(errorCode).send('Something broke!')
    return
  }

  res.send('Success!')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
