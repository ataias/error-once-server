# error-once-server

This repo has two servers: an express rest server and a GraphQL Apollo Server.
They have a very weird API that accepts the status code to be returned. In the
case the status code is in the range `[400, 600)`, the request will fail.
However, for a few seconds after such a failure, you can retry the request and
get a successful response.

The idea of this dummy api was to test
[axios-retry](https://www.npmjs.com/package/axios-retry) and the
[Apollo Client Retry Link](https://www.apollographql.com/docs/react/api/link/apollo-link-retry).
